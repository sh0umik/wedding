package com.wedding.joshita.wedding;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class HoludImageSlider extends AppCompatActivity {
    private ViewPager viewPager;
    HoludSwipeAdpater holudSwipeAdpater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.holud_image_slider);
        setTitle("Holud");
        viewPager = (ViewPager) findViewById(R.id.Hpager);
        holudSwipeAdpater = new HoludSwipeAdpater(this);
        viewPager.setAdapter(holudSwipeAdpater);
    }
}
