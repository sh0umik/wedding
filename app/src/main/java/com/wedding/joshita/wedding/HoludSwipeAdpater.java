package com.wedding.joshita.wedding;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Photo;
import com.sromku.simple.fb.entities.Privacy;
import com.sromku.simple.fb.listeners.OnPublishListener;

/**
 * Created by y34h1a on 1/16/16.
 */
public class HoludSwipeAdpater extends PagerAdapter {
    private int[] image_resources = {R.drawable.holud_1, R.drawable.holud_2,R.drawable.holud_3, R.drawable.holud_4,R.drawable.holud_5};
    private int[] image_price = {2000, 3455, 2300, 3200, 3499};
    private Context context;
    private LayoutInflater inflater;


    public HoludSwipeAdpater(Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return image_resources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (RelativeLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View item_view = inflater.inflate(R.layout.holud_swipe_layout,container,false);

        final ImageView imageView = (ImageView) item_view.findViewById(R.id.ivSliderImage);
        final TextView textView = (TextView) item_view.findViewById(R.id.tvSliderImageText);
        Button share = (Button) item_view.findViewById(R.id.facebookShare);
        imageView.setImageResource(image_resources[position]);
        textView.setText("Price:" + image_price[position]);
        container.addView(item_view);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // set privacy
                Privacy privacy = new Privacy.Builder()
                        .setPrivacySettings(Privacy.PrivacySettings.SELF)
                        .build();
                Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                // create Photo instance and add some properties
                Photo photo = new Photo.Builder()
                        .setImage(bitmap)
                        .setName(textView.getText().toString())
                        .setPrivacy(privacy)
                        .build();

                SimpleFacebook.getInstance().publish(photo, false, new OnPublishListener() {

                    @Override
                    public void onException(Throwable throwable) {

                    }

                    @Override
                    public void onFail(String reason) {
                        Toast.makeText(context, reason, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onThinking() {
                        Toast.makeText(context, "Posting", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onComplete(String response) {
                        Toast.makeText(context,"Posted",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


        return item_view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
