package com.wedding.joshita.wedding;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Photo;
import com.sromku.simple.fb.entities.Privacy;
import com.sromku.simple.fb.listeners.OnPublishListener;

/**
 * Created by y34h1a on 1/16/16.
 */
public class ReceptionSwipeAdpater extends PagerAdapter {
    private int[] image_resources = {R.drawable.reception_1, R.drawable.reception_2,R.drawable.reception_3, R.drawable.reception_4,R.drawable.reception_5,R.drawable.reception_6,R.drawable.reception_7};
    private int[] image_price = {2300, 3200, 3499, 4500, 3100, 2700, 5200};
    private Context context;
    private LayoutInflater inflater;

    public ReceptionSwipeAdpater(Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return image_resources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (RelativeLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View item_view = inflater.inflate(R.layout.reception_swipe_layout,container,false);
        final ImageView imageView = (ImageView) item_view.findViewById(R.id.ivSliderImage);
        imageView.setImageResource(image_resources[position]);
        Button share = (Button) item_view.findViewById(R.id.facebookShare);
        final TextView textView = (TextView) item_view.findViewById(R.id.tvSliderImageText);
        textView.setText("Price:" + image_price[position]);
        container.addView(item_view);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // set privacy
                Privacy privacy = new Privacy.Builder()
                        .setPrivacySettings(Privacy.PrivacySettings.SELF)
                        .build();
                Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                // create Photo instance and add some properties
                Photo photo = new Photo.Builder()
                        .setImage(bitmap)
                        .setName(textView.getText().toString())
                        .setPrivacy(privacy)
                        .build();

                SimpleFacebook.getInstance().publish(photo, false, new OnPublishListener() {

                    @Override
                    public void onException(Throwable throwable) {

                    }

                    @Override
                    public void onFail(String reason) {
                        Toast.makeText(context, reason, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onThinking() {
                        Toast.makeText(context, "Posting", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onComplete(String response) {
                        Toast.makeText(context, "Posted", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        return item_view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
