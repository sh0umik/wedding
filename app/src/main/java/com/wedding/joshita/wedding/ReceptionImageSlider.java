package com.wedding.joshita.wedding;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

public class ReceptionImageSlider extends AppCompatActivity {
    private ViewPager viewPager;
    ReceptionSwipeAdpater receptionSwipeAdpater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reception_image_slider);
        setTitle("Reception");
        viewPager = (ViewPager) findViewById(R.id.Rpager);
        receptionSwipeAdpater = new ReceptionSwipeAdpater(this);
        viewPager.setAdapter(receptionSwipeAdpater);
    }
}
