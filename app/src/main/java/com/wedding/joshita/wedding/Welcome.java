package com.wedding.joshita.wedding;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.wedding.joshita.wedding.Facebook.Utils;

import java.util.List;

public class Welcome extends AppCompatActivity {
    Context context;
    private SimpleFacebook mSimpleFacebook;
    private Button enter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utils.updateLanguage(getApplicationContext(), "en");
        Utils.printHashKey(getApplicationContext());
        mSimpleFacebook = SimpleFacebook.getInstance(this);

        setContentView(R.layout.activity_welcome);

        context = getApplicationContext();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        enter = (Button) findViewById(R.id.enter);

        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSimpleFacebook.isLogin()) {
                    setLogin();
                } else {
                    Intent i = new Intent(getApplicationContext(), Category2.class);
                    startActivity(i);
                }
            }
        });

    }

    private void setLogin() {
        // Login listener
        final OnLoginListener onLoginListener = new OnLoginListener() {

            @Override
            public void onFail(String reason) {

            }

            @Override
            public void onException(Throwable throwable) {

            }

            @Override
            public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
                // change the state of the button or do whatever you want
                Toast.makeText(getApplicationContext(), "Successfully Logged In", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), Category2.class);
                startActivity(i);
                finish();
            }

            @Override
            public void onCancel() {

            }

        };
        mSimpleFacebook.login(onLoginListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    protected void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(this);
    }

}
